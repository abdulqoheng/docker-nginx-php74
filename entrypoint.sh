#!/bin/sh

# Ubah kepemilikan direktori storage dan bootstrap/cache
chown -R nginx:nginx /usr/share/nginx/html/storage /usr/share/nginx/html/bootstrap/cache

# Eksekusi perintah yang diberikan sebagai argumen ke skrip ini
exec "$@"
